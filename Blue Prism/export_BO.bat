@REM ====================================================================================
@REM
@REM Set environment variable BLUEPRISMPATH = path to your local Blue Prism installation
@REM And create Blue Prism local user gitimport with password gitimport
@REM
@REM ====================================================================================

cd "Business Objects"
for /F "tokens=*" %%A in (..\BusinessObjectList.txt) do AutomateC /export "%%A" /user admin admin
cd ..
